#!/bin/bash
#
# invcol - 1.0
#
# Inventory collector tool
#
# Usage: Running the script, they'll create a /tmp/ file with the machine
#  		inventory, a la aix's snap
#
# Tools needed for proper data colection:
#       lspci, lsusb, lvm2 (if applicable), efibootmgr (if applicable) and systemd (if applicable)

# Check if is running as root
if [[ $EUID -ne 0 ]]; then
	echo "";
	echo "invcol must be run as root";
	echo "";
	exit 1
fi

# Create a /tmp directory to collect stuff
echo -e "\e[1m\e[93m 1.  \e[97mSetting variables and creating directories...";
	mkdir /tmp/invcol
	TMPDIR='/tmp/invcol/'
	TIME=`date +%G%m%e-%H%M`
	mkdir $TMPDIR/$TIME

# PCI Report
echo -e "\e[1m\e[93m 2.  \e[97mGathering PCI information...";
	lspci > $TMPDIR/$TIME/lspci.txt
	lspci -vvv > $TMPDIR/$TIME/lspci-verbose.txt 2>/dev/null

# USB Report
echo -e "\e[1m\e[93m 3.  \e[97mGathering USB infomation...";
	lsusb > $TMPDIR/$TIME/lsusb.txt
	lsusb -vvv > $TMPDIR/$TIME/lsusb-verbsose.txt 2>/dev/null

# Processing Report
echo -e "\e[1m\e[93m 4.  \e[97mGathering CPU information...";
	cat /proc/cpuinfo > $TMPDIR/$TIME/cpuinfo.txt

# Memory Report
echo -e "\e[1m\e[93m 5.  \e[97mGathering memory information...";
	cat /proc/meminfo > $TMPDIR/$TIME/meminfo.txt
	free -h > $TMPDIR/$TIME/free-memory.txt

# Zoneinfo
echo -e "\e[1m\e[93m 6.  \e[97mGathering zoneinfo information...";
	cat /proc/zoneinfo > $TMPDIR/$TIME/zoneinfo.txt

# VMstat
echo -e "\e[1m\e[93m 7.  \e[97mGathering vmstat information...";
	cat /proc/vmstat > $TMPDIR/$TIME/vmstat.txt

# Kernel Version
echo -e "\e[1m\e[93m 8.  \e[97mGathering kernel information...";
	cat /proc/version > $TMPDIR/$TIME/version.txt

# Timer List
echo -e "\e[1m\e[93m 9.  \e[97mGathering timer list...";
	cat /proc/timer_list > $TMPDIR/$TIME/timer_list.txt

# Stat
echo -e "\e[1m\e[93m10.  \e[97mGathering stats...";
	cat /proc/stat > $TMPDIR/$TIME/stat.txt

# SoftIRQs
echo -e "\e[1m\e[93m11.  \e[97mGathering softIRQs...";
	cat /proc/softirqs > $TMPDIR/$TIME/softirqs.txt

# Slabinfo
echo -e "\e[1m\e[93m12.  \e[97mGathering slabinfo...";
	cat /proc/slabinfo > $TMPDIR/$TIME/slabinfo.txt

# Schedstat
echo -e "\e[1m\e[93m13.  \e[97mGathering scheduler stats...";
	cat /proc/schedstat > $TMPDIR/$TIME/schedstat.txt

# Kernel modules
echo -e "\e[1m\e[93m14.  \e[97mGathering kernel modules...";
	cat /proc/modules > $TMPDIR/$TIME/kernel-modules.txt

# Misc modules
echo -e "\e[1m\e[93m15.  \e[97mGathering MISC modules...";
	cat /proc/misc > $TMPDIR/$TIME/misc-modules.txt

# IO Ports
echo -e "\e[1m\e[93m16.  \e[97mGathering IO Ports information...";
	cat /proc/ioports > $TMPDIR/$TIME/ioports.txt

# IO Mem
echo -e "\e[1m\e[93m17.  \e[97mGathering IO Mem information...";
	cat /proc/iomem > $TMPDIR/$TIME/iomem.txt

# Interrupts
echo -e "\e[1m\e[93m18.  \e[97mGathering Interrupts information...";
	cat /proc/interrupts > $TMPDIR/$TIME/interrupts.txt

# Disk Stats
echo -e "\e[1m\e[93m19.  \e[97mGathering diskstats information...";
	cat /proc/diskstats > $TMPDIR/$TIME/diskstats.txt

# Crypto info
echo -e "\e[1m\e[93m20.  \e[97mGathering crypto information...";
	cat /proc/crypto > $TMPDIR/$TIME/crypto-info.txt

# Kernel Configuration
echo -e "\e[1m\e[93m21.  \e[97mGathering kernel configuration...";
	zcat /proc/config.gz > $TMPDIR/$TIME/kernel-config.txt

# LVM Dump
echo -e "\e[1m\e[93m22.  \e[97mGathering LVM information...";
	lvmdump -s -l -p -m -a -u -d $TMPDIR/$TIME/LVM/ 1> /dev/null 2> /dev/null

# Fstab
echo -e "\e[1m\e[93m23.  \e[97mGathering fstab information...";
	cat /etc/fstab > $TMPDIR/$TIME/fstab.txt

# Modules information and blacklists
echo -e "\e[1m\e[93m24.  \e[97mGathering modules information and blacklists...";
	cp -R /etc/modprobe.d $TMPDIR/$TIME/

# dmesg
echo -e "\e[1m\e[93m25.  \e[97mGathering dmesg...";
	dmesg > $TMPDIR/$TIME/dmesg.txt

# systemd log
echo -e "\e[1m\e[93m26.  \e[97mGathering journalctl logs...";
	journalctl -p 7 -b > $TMPDIR/$TIME/journalctl.txt

# Boots
echo -e "\e[1m\e[93m27.  \e[97mGathering boots...";
	journalctl --list-boots > $TMPDIR/$TIME/boots.txt

# EFI
echo -e "\e[1m\e[93m28.  \e[97mGathering EFI boot configurations...";
	efibootmgr -v > $TMPDIR/$TIME/efibootmgr.txt
	cp /boot/grub/grub.cfg $TMPDIR/$TIME/grub.cfg 2>/dev/null

# Copy any other logs, in case of not using systemd
echo -e "\e[1m\e[93m29.  \e[97mGathering any other logs...";
	cp /var/log $TMPDIR/$TIME/

# Compressing
echo -e "\e[1m\e[93m210.  \e[97mCreating tarball...";
	cd /tmp/invcol
	tar cjf $TMPDIR/$TIME.tar.bz2 $TIME

# Finish
echo -e "\e[1m\e[93m30.  \e[97mDone! Your tarball is available at "$TMPDIR/$TIME.tar.bz2

